# Uploader

## dependencies
```npm install```

## run
```node index.js```

## uploader docker image

### gitlab
```docker login registry.gitlab.com```

```docker build -t uploader .```

```docker tag uploader uploader:latest```

```docker tag uploader registry.gitlab.com/manuel.deval/uploader:latest```

```docker push registry.gitlab.com/manuel.deval/uploader:latest```