
// Dependencies
var express = require('express');
var Keycloak = require('keycloak-connect');
var hogan = require('hogan-express');
var session = require('express-session');
var upload = require('./upload.js')

// Configurable stuff
const config = { 
    port: process.env.PORT || 3000,
    uploadFolder: process.env.UPLOAD_FOLDER || "./upload/",
    sessionSecret: process.env.SESSION_SECRET || "17Zjqj6qlLQmsu0mqsdP",
    keycloakRealm: process.env.KEYCLOAK_REALM || "OAIDC",
    keycloakAuthServerUrl: process.env.KEYCLOAK_AUTH_SERVER_URL  || "http://localhost:8080/auth",
    keycloakSslRequired: process.env.KEYCLOAK_SSL_REQUIRED || "external",
    keycloakResource: process.env.KEYCLOAK_RESOURCE || "Uploader",
    keycloakPublicClient: process.env.KEYCLOAK_PUBLIC_CLIENT || "Uploader",
    keycloakConfidentialPort: process.env.KEYCLOAK_CONFIDENTIAL_PORT || 0,
    externalDomain: process.env.EXTERNAL_DOMAIN || "http://localhost:3000",
    keycloakUploadRole: "chargement_fichier"
}

// Http server
var app = express();
const server = app.listen(config.port, () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Listening at http://%s:%s', host, port);
});


// Security setup
app.use(session({
  secret: config.sessionSecret,                         
  resave: false,                         
  saveUninitialized: true,                         
  store: memoryStore                       
}));                                               
var memoryStore = new session.MemoryStore();                       
let kcConfig = {
  "realm":              config.keycloakRealm,
  "auth-server-url":    config.keycloakAuthServerUrl,
  "ssl-required":       config.keycloakSslRequired,
  "resource":           config.keycloakResource,
  "public-client":      config.keycloakPublicClient,
  "confidential-port":  config.keycloakConfidentialPort
};
var keycloak = new Keycloak({ store: memoryStore },kcConfig);  
app.use(keycloak.middleware());
const parseGrant = grant => {
  return {
    user: grant.access_token.content.preferred_username,
    resourceAccess: grant.access_token.content.resource_access
  }
}

// Server side templating (views)
app.set('view engine', 'html');
app.set('views', require('path').join(__dirname, '/view'));
app.engine('html', hogan);
app.get('/', keycloak.protect(config.keycloakUploadRole), function (req, res) {
  userInfos = parseGrant(req.kauth.grant)
  res.render('index', { userInfos, config })
});
app.use('/static', express.static('public'));

// Upload url
app.post('/upload', keycloak.protect(config.keycloakUploadRole), upload(config) );
