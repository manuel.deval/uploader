FROM node:carbon

# TOTO npm en cas de proxy
#ENV HTTP_PROXY=http://10.237.7.16:3128
#ENV HTTPS_PROXY=http://10.237.7.16:3128

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY *.js ./
COPY public ./public
COPY view ./view

EXPOSE 3000
CMD [ "node", "index.js" ]
