const multiparty = require('multiparty');
const fs = require('fs');

const removeFile = fileName => {
  fs.exists(fileName, (exists) => {
    if (exists){
      fs.unlink(fileName, (err) => { if (err) console.log("Unable to delete: ",fileName) })
    }
  })
}

const renameFile = (f1,f2) => {
  fs.rename(f1,f2, (err) => {
    if (err) {
      console.log('Rename error:',err)
    } 
  });
}

const partHandler = config => part => {
  var file = undefined;
  if (!part.filename) {
    part.resume();
  } else {
    const filePartName = config.uploadFolder+"/"+part.filename+".part"
    const fileName = config.uploadFolder+"/"+part.filename
    console.log("Begin => Field:"+part.name+" / FileName:"+part.filename)
    file = fs.createWriteStream(filePartName);
    file.on('open' ,() => part.pipe(file))
    file.on('close',() => {
      renameFile(filePartName,fileName)
      console.log("End => Field:"+part.name+" / FileName:"+part.filename)
    })
    part.on('error',err => {
      if (file){
        file.end();
      }
      removeFile(filePartName)
      removeFile(fileName)
    })
  }
}

const formFieldHandler =  (fieldName, fieldValue) => {
  console.log(fieldName, fieldValue)
}

const sendJson = status => object => res => { 
  res.status(status)
  res.send(JSON.stringify(object))
}
const send200 = sendJson(200)
const send400 = sendJson(400)

const formClosedHandler = res => () => send200({status:"Ok"})(res)
const formErrordHandler = res => () => send400({status:"Error"})(res)

const upload = (config) => (req, res) => {
  var form = new multiparty.Form()
  form.on('part' , partHandler(config) )
	form.on('field', formFieldHandler )
  form.on('close', formClosedHandler(res) )
  form.on('error', formErrordHandler(res) )
	form.parse(req);
}

module.exports = upload;